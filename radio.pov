#version 3.7;

global_settings {
  assumed_gamma 1.0
  radiosity {    
    pretrace_start 0.04
    pretrace_end 0.01
    count 200
    recursion_limit 3
    nearest_count 10
    error_bound 0.1
  }         
  max_trace_level 25
  photons {
    //spacing 0.0005
    //autostop 0
    //jitter 0      
    //save_file "radio.ph"
    load_file "radio.ph"
  }  
  subsurface { radiosity true }
}  

camera {location <1.5,1.5,2> look_at <0,-.5,0> up <0,1,0> right <1.6,0,0> }            

plane{y,-.5
    pigment{rgb 1}   
    finish { reflection <.8,.15,.15>}
    photons { target reflection on }

}  
difference{
    box{-.5, .5}
    sphere{0,.26}
    pigment{rgbf <.3,.3,1,.9> }   
    photons{
        target
        refraction on  
        reflection on
        //collect off   
    }  
    interior {
        ior 1.5
        fade_distance 1
        fade_power 1001 
 
    }    
    finish {subsurface { translucency rgb .1 }}
}   
 
#declare Lightbulb = sphere{
    <0,.4,-.2>, .05
    pigment{ rgb 1} 
    finish{ diffuse 0 emission 1}     
    no_shadow
}  
light_source{
    <0,.4,-.2>
    color rgb 1
    looks_like{Lightbulb}          
}